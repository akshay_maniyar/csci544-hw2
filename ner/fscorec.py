import sys
f=open(sys.argv[1],'r')
p=open(sys.argv[2],'r')
corrl = f.read().splitlines()
out = p.read().splitlines()
correct = 0
count = 0
countper=0
countorg=0
countmisc=0
countloc=0
corloc=0
corper=0
cororg=0
cormisc=0
classloc=0
classper=0
classorg=0
classmisc=0
classcount=0



for i in range(len(corrl)):
    if(corrl[i][0]=="B"):
        count=count+1
    if(corrl[i]=="B-ORG"):
        countorg=countorg+1
    if(corrl[i]=="B-MISC"):
        countmisc=countmisc+1
    if(corrl[i]=="B-PER"):
        countper=countper+1
    if(corrl[i]=="B-LOC"):
        countloc=countloc+1    

    if(out[i][0]=="B"):
        classcount=classcount+1
    if(out[i]=="B-ORG"):
        classorg=classorg+1
    if(out[i]=="B-MISC"):
        classmisc=classmisc+1
    if(out[i]=="B-PER"):
        classper=classper+1
    if(out[i]=="B-LOC"):
        classloc=classloc+1

    if(corrl[i]==out[i]) and (corrl[i]!="O")and(corrl[i][0]=="B"):
        print("new ner started")
        print(corrl[i],out[i])
        for k in range(i+1,len(corrl)):
            if(corrl[k]!=out[k]):
                print("bad ner")
                i=k
                break
            if(corrl[k]==out[k]) and ((corrl[k][0]=="O") or (corrl[k][0]=="B")):
                print("correct sequnece recieved")
                print(corrl[k],out[k])
                correct = correct+1;
                if(corrl[i]=="B-ORG"):
                    cororg=cororg+1
                if(corrl[i]=="B-MISC"):
                    cormisc=cormisc+1
                if(corrl[i]=="B-PER"):
                    corper=corper+1
                if(corrl[i]=="B-LOC"):
                    corloc=corloc+1
                i=k
                break

perpre = corper/classper
orgpre = cororg/classorg
miscpre = cormisc/classmisc
locpre = corloc/classloc

perrec = corper/countper
orgrec = cororg/countorg
miscrec = cormisc/countmisc
locrec = corloc/countloc

f1per = (2*perpre*perrec)/(perpre+perrec)
f1org = (2*orgpre*orgrec)/(orgpre+orgrec)
f1misc = (2*miscpre*miscrec)/(miscpre+miscrec)
f1loc = (2*locpre*locrec)/(locpre+locrec)
print("complete " + "Precision: "+str(correct/classcount)+" Recall: "+str(correct/count))
print("acc " + str(correct/count))
print("B-ORG " + "Precision: "+str(cororg/classorg)+" Recall: "+str(cororg/countorg)+" F1 "+str(f1org))
print("B-PER " + "Precision: "+str(corper/classper)+" Recall: "+str(corper/countper)+" F1 "+str(f1per))
print("B-MISC " + "Precision: "+str(cormisc/classmisc)+" Recall: "+str(cormisc/countmisc)+" F1 "+str(f1misc))
print("B-LOc " + "Precision: "+str(corloc/classloc)+" Recall: "+str(corloc/countloc)+ " F1 "+str(f1loc))

