import sys
f = open(sys.argv[1],'r')
p = open(sys.argv[2],'r')
f2 = open(sys.argv[3],'w')
f2.truncate()
f2 = open(sys.argv[3],'a')
lines = f.read().splitlines()
classes = []
for line in lines:
    firstword = line.split()[0]
    if firstword not in classes:
        classes.append(firstword)

#print (classes)

weight_vec = [{} for _ in range(len(classes))]

for i in range(len(classes)):
    words = lines[i].split()
    for word in words[1:]:
        keyval = word.split(':break:')
        weight_vec[i][keyval[0]] = float(keyval[1])

        
#print(weight_vec)

lines = p.read().splitlines()

count = 0
correct = 0
for line in lines:
    #print(line)
    count=count+1
    prob = [0]*len(classes)
    for i in range(len(classes)):
        words = line.split()
        for word in words[1:]:
            if word in weight_vec[i]:
                prob[i] = prob[i]+weight_vec[i][word]
    index = prob.index(max(prob))
    f2.write(classes[index]+"\n")
    if(classes[index]==words[0]):
    	correct=correct+1


print("accuracy "+str(correct/count))

