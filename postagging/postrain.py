import sys, argparse
from subprocess import call


parser = argparse.ArgumentParser(conflict_handler='resolve')
#parser = argparse.ArgumentParser()
parser.add_argument("trainingfile",help="dev file name")
parser.add_argument("modelfile",help="dev file name")
parser.add_argument("-h","--devfile",help="dev file name")
args = parser.parse_args()

f=open(args.trainingfile,'r')

p=open('pos_train_formatted','w')
p.truncate()
p=open('pos_train_formatted','a')

lines = f.read().splitlines()
for line in lines:
    line = "BOS "+line+" EOS"
    words = line.split()
    for i in range(1,len(words)-1):
        p.write(words[i].split("/")[1]+" curr_"+words[i].split("/")[0]+" prev_"+words[i-1].split("/")[0]+" next_"+words[i+1].split("/")[0])
        if words[i].split("/")[0].isupper():
            p.write(" WSIG_A")
        elif words[i].split("/")[0]=='':
            p.write(" WSIG_e")
        elif words[i].split("/")[0][0].isupper():
            p.write(" WSIG_Aa")
        elif words[i].split("/")[0].isdigit():
            p.write(" WSIG_d")
        elif not(words[i].split("/")[0].isupper()):
            p.write(" WSIG_a")
        else:
            p.write(" WSIG_")

        if words[i-1].split("/")[0].isupper():
            p.write(" WSIGp_A")
        elif words[i-1].split("/")[0]=='':
            p.write(" WSIGp_e")    
        elif words[i-1].split("/")[0][0].isupper():
            p.write(" WSIGp_Aa")
        elif words[i-1].split("/")[0].isdigit():
            p.write(" WSIGp_d")
        elif not(words[i-1].split("/")[0].isupper()):
            p.write(" WSIGp_a")
        else:
            p.write(" WSIGp_")

        if words[i+1].split("/")[0].isupper():
            p.write(" WSIGn_A")
        elif words[i+1].split("/")[0]=='':
            p.write(" WSIGn_e")    
        elif words[i+1].split("/")[0][0].isupper():
            p.write(" WSIGn_Aa")
        elif words[i+1].split("/")[0].isdigit():
            p.write(" WSIGn_d")
        elif not(words[i+1].split("/")[0].isupper()):
            p.write(" WSIGn_a")
        else:
            p.write(" WSIGn_")

        p.write("\n")    
 
if args.devfile:
    f=open(args.devfile,'r')

    p=open('pos_dev_formatted','w')
    p.truncate()
    p=open('pos_dev_formatted','a')
    
    lines = f.read().splitlines()
    for line in lines:
        line = "BOS "+line+" EOS"
        words = line.split()
        for i in range(1,len(words)-1):
            p.write(words[i].split("/")[1]+" curr_"+words[i].split("/")[0]+" prev_"+words[i-1].split("/")[0]+" next_"+words[i+1].split("/")[0])

            if words[i].split("/")[0].isupper():
                p.write(" WSIG_A")
            elif words[i].split("/")[0]=='':
                p.write(" WSIG_e")
            elif words[i].split("/")[0][0].isupper():
                p.write(" WSIG_Aa")
            elif words[i].split("/")[0].isdigit():
                p.write(" WSIG_d")
            elif not(words[i].split("/")[0].isupper()):
                p.write(" WSIG_a")
            else:
                p.write(" WSIG_")

            if words[i-1].split("/")[0].isupper():
                p.write(" WSIGp_A")
            elif words[i-1].split("/")[0]=='':
                p.write(" WSIGp_e")    
            elif words[i-1].split("/")[0][0].isupper():
                p.write(" WSIGp_Aa")
            elif words[i-1].split("/")[0].isdigit():
                p.write(" WSIGp_d")
            elif not(words[i-1].split("/")[0].isupper()):
                p.write(" WSIGp_a")
            else:
                p.write(" WSIGp_")

            if words[i+1].split("/")[0].isupper():
                p.write(" WSIGn_A")
            elif words[i+1].split("/")[0]=='':
                p.write(" WSIGn_e")    
            elif words[i+1].split("/")[0][0].isupper():
                p.write(" WSIGn_Aa")
            elif words[i+1].split("/")[0].isdigit():
                p.write(" WSIGn_d")
            elif not(words[i+1].split("/")[0].isupper()):
                p.write(" WSIGn_a")
            else:
                p.write(" WSIGn_")

            p.write("\n")   
if args.devfile:       
    call(["python3","../perceplearn.py","pos_train_formatted",sys.argv[2],"-h","pos_dev_formatted"])

else:
    call(["python3","../perceplearn.py","pos_train_formatted",sys.argv[2]])
        
        
        
        
        
    
