import sys

f = open(sys.argv[1],'r')
lines = f.read().splitlines()
classes = []
for line in lines:
    firstword = line.split()[0]
    if firstword not in classes:
        classes.append(firstword)

#print (classes)

weight_vec = [{} for _ in range(len(classes))]

for i in range(len(classes)):
    words = lines[i].split()
    for word in words[1:]:
        keyval = word.split(':break:')
        weight_vec[i][keyval[0]] = float(keyval[1])

count = 0
correct = 0        
for line in sys.stdin:
        line = "BOS "+line+" EOS"
        words = line.split()
        #print(line)
        for i in range(1,len(words)-1):
            line_formatted = "curr_"+words[i]+" prev_"+words[i-1]+" next_"+words[i+1]
            if words[i].split("/")[0].isupper():
                line_formatted = line_formatted+ " WSIG_A"
            elif words[i].split("/")[0]=='':
                line_formatted = line_formatted+ " WSIG_e"    
            elif words[i].split("/")[0][0].isupper():
                line_formatted = line_formatted+ " WSIG_Aa"
            elif words[i].split("/")[0].isdigit():
                line_formatted = line_formatted+ " WSIG_d"
            elif not(words[i].split("/")[0].isupper()):
                line_formatted = line_formatted+ " WSIG_a"
            else:
                line_formatted = line_formatted+ " WSIG_"

            if words[i-1].split("/")[0].isupper():
                line_formatted = line_formatted+ " WSIGp_A"
            elif words[i-1].split("/")[0]=='':
                line_formatted = line_formatted+ " WSIGp_e"    
            elif words[i-1].split("/")[0][0].isupper():
                line_formatted = line_formatted+ " WSIGp_Aa"
            elif words[i-1].split("/")[0].isdigit():
                line_formatted = line_formatted+ " WSIGp_d"
            elif not(words[i-1].split("/")[0].isupper()):
                line_formatted = line_formatted+ " WSIGp_a"
            else:
                line_formatted = line_formatted+ " WSIGp_"

            if words[i+1].split("/")[0].isupper():
                line_formatted = line_formatted+ " WSIGn_A"
            elif words[i+1].split("/")[0]=='':
                line_formatted = line_formatted+ " WSIGn_e"    
            elif words[i+1].split("/")[0][0].isupper():
                line_formatted = line_formatted+ " WSIGn_Aa"
            elif words[i+1].split("/")[0].isdigit():
                line_formatted = line_formatted+ " WSIGn_d"
            elif not(words[i+1].split("/")[0].isupper()):
                line_formatted = line_formatted+ " WSIGn_a"
            else:
                line_formatted = line_formatted+ " WSIGn_"

            line_formatted = line_formatted+ "\n"
            count=count+1
            prob = [0]*len(classes)
            for j in range(len(classes)):
                words_formatted = line_formatted.split()
                for word in words_formatted:
                    if word in weight_vec[j]:
                        prob[j] = prob[j]+weight_vec[j][word]
            index = prob.index(max(prob))
            sys.stdout.write(words_formatted[0].split("_")[1]+"/"+classes[index]+" ")
        sys.stdout.write("\n")        
        
        
