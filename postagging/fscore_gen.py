import sys
correct_out = open(sys.argv[1],'r')
my_out = open(sys.argv[2],'r')

correct_tags = correct_out.read().splitlines()
my_tags = my_out.read().splitlines()

print(len(my_tags))
print(len(correct_tags))
classes = []

for tag in correct_tags:
    if tag not in classes:
        classes.append(tag)

count = 0
correct = 0
no_of_correct = [0]*len(classes)
no = [0]*len(classes)
precision = [0]*len(classes)
recall = [0]*len(classes)
f1 = [0]*len(classes)
classified = [0]*len(classes)

for j in range(len(correct_tags)):
    count=count+1
    for i in range(len(classes)):
        if(correct_tags[j]==classes[i]):
            index = i
    for i in range(len(classes)):
        if(my_tags[j]==classes[i]):
            myindex = i

    no[index] = no[index]+1
    classified[index]=classified[index]+1
    if(correct_tags[j]==my_tags[j]):
        no_of_correct[index]=no_of_correct[index]+1
        correct=correct+1

print(no_of_correct)
for i in range(len(classes)):
    precision[i] = no_of_correct[i]/classified[i]
    recall[i] = no_of_correct[i]/no[i]
    try:
        f1[i] = (2*precision[i]*recall[i]/(precision[i]+recall[i]))
    except:
        print(classes[i]+" 0 classified correctlt")
    print(str(classes[i])+" precision: "+str(precision[i])+" recall: "+str(recall[i])+" F1 score: "+str(f1[i]))
print("accuracy "+str(correct/count))
        
